////////////////////////////////////////////////////////////
//Debugging
////////////////////////////////////////////////////////////
//Verbose Mode
//Set to true if you want to have a bunch of debugging information on the console.
//Default: false (Disabled)
global.verbose = ( false );

////////////////////////////////////////////////////////////
//Frontend
////////////////////////////////////////////////////////////
//Discord Status
//Set the status used by the bot upon launch.
//For more advanced users, you can replace the string with a function to have something more dynamic.
//NOTE: It will only properly set if you either set it yourself in the function or if it returns a string.
global.discordStatus = "";
//Discord Avatar
//Specify the URL to a custom avatar here.
//Alternatively, you can specify a filepath and the bot will try to load the avatar at that filepath.
global.discordAvatar = "";

////////////////////////////////////////////////////////////
//Admin Information
////////////////////////////////////////////////////////////
//Bot Host
//Your own userID, aka the one that's owning the console. Paste your user ID into the quotation marks
//	Right click yourself (in developer mode) > Copy ID
//If an error occurs, the bot will ping or DM you telling you to check console
global.botHost = ( "" );
//Bot Admins
//If you wish to add other bot admins, add them here, similar way to the first.
//	Right click a desired admin (in developer mode) > Copy ID
//Add a new line if you wish to have more than one admin for some more specific commands.
//This is not equivalent to server admins. Odds are, if someone isn't managing the bot, they don't need this permission.
global.botAdmins = [
	( "" ),
];

////////////////////////////////////////////////////////////
//Discord API
////////////////////////////////////////////////////////////
//Bot Token
//I'll assume you know what this is.
global.token = ( "" );
//Default Command Prefix
//What to type before the commands to make them parse properly.
//Can be changed on a per-server basis, but it'll default to this
//Default: ">>"
//NOTE: My personal copy uses ">>>" so don't use that
global.defaultPrefix = ( ">>" );

////////////////////////////////////////////////////////////
//Pixiv API
////////////////////////////////////////////////////////////
//Pixiv API Token
//Specify the refresh token for the account you want to do the searching here.
//This is what's used to authorize the bot for pixiv searching
//It has to be valid, unfortunately :(
global.pixivRefreshToken =  ( "" );

































//Don't change this.
global.configVer = "2.0.0";