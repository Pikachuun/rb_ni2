'use strict';
console.log("loading main client loop...");

global.commandParser = async function (msg, toParse, args, post, prefix, guildInfo) {
	let m = null;
	//if the command doesn't exist or the command can't be called
	if (!commands[toParse] || commands[toParse].uncallable) {
		if (verbose) { console.log(`Invalid/unusable command ${toParse} detected, abandoning...`); }
		m = await msg.reply(`That isn't a valid command. Maybe \`${prefix}help\` would be useful for you.`);
		setTimeout(async () => { await m.delete(); }, 5000);
		return;
	}
	let command = commands[toParse];
	//If the command requires bot admin and the user isn't one
	if (command.requiresBotAdmin && msg.author.id != botHost && botAdmins.indexOf(msg.author.id) === -1) {
		if (verbose) { console.log(`Admin command ${toParse} used by normal user, abandoning...`); }
		m = await msg.reply("I can't let you execute that, sorry.");
		setTimeout(async () => { await m.delete(); }, 5000);
		return;
	}
	//If the user of the command doesn't have the required permissions to use it
	if (command.requiredPerms) {
		if (!msg.guild) {
			if (verbose) { console.log(`Server command ${toParse} used in non-server, abandoning...`); }
			m = await msg.reply("This is not a server, so I can't execute that.");
			setTimeout(async () => { await m.delete(); }, 5000);
			return;
		}
		let author = await msg.guild.members.fetch(msg.author.id);
		for (let p of command.requiredPerms) {
			if (!author.permissions.has(p)) { 
				if (verbose) { console.log(`Command ${toParse} used by a user without the ${p} permission, abandoning...`); }
				m = await msg.reply("I can't let you execute that, sorry.");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
		}
	}
	//Execute the command
	if (verbose) { console.log(`Parsing command ${toParse}...`); }
	command.exec(msg, args, post, prefix, guildInfo)
		.catch(async e => {
			console.log(e);
			let tmpFlagS = false;
			if (msg.channel.members && msg.channel.members.get(botHost)) { //Non-DM Channel with the host
				await msg.channel.send(`${msg.channel.members.get(botHost)}, this guy broke something or Chooniel made an absolutely god-tier typo, check console`)
					.then(() => { tmpFlagS = true; })
					.catch(e => { console.log(`Nested Error Oh Fuck Why\n${e}`); });
			}
			if (!tmpFlagS) { //message send failure
				let host = await CLIENT.users.fetch(botHost);
				host.send(`Something broke while parsing the below command. Check console\n\`${toParse} ${args.join(" ")}\``)
					.catch(e => { console.log(`Nested Error in DM Oh Fuck Why\n${e}`); });
			}
		});
}

CLIENT.on('messageCreate', async message => {
	let msg = message;
	if (message.partial) {
		await message.fetch()
			.then(m => { msg = m; })
			.catch(e => { msg = null; if (verbose) { console.log(`Message fetching failed. Cannot parse.\n${e}`); }});
		if (!msg) { return; }
	}
	if (msg.author.bot) { return; } //Precaution
	if (msg.guild && !(msg.channel.viewable && msg.channel.members.get(CLIENT.user.id) && msg.channel.members.get(CLIENT.user.id).permissionsIn(msg.channel).has("SEND_MESSAGES"))) { return; } //If you can't send messages to the channel, don't bother
	if (verbose) {
		if (msg.guild) {
			console.log(`Message ID ${msg.id} created in guild &${msg.guild.id}, channel #${msg.channel.id}`);
		} else {
			console.log(`Message ID ${msg.id} created in channel #${msg.channel.id}`);
		}
	}
	let guildInfo = {};
	if (msg.guild) {
		guildInfo = fetchGuildKey(msg.guild.id, "mainInfo");
		if (!guildInfo) {
			guildInfo = {};
			setGuildKey(msg.guild.id, "mainInfo", {}); //Bind to database
		}
		let prompt = PROMPTS[msg.guild.id];
		if (prompt && prompt.isActive && prompt.onMessage && msg.author.id === prompt.promptee && msg.channel.id === prompt.channel) {
			let args = msg.content.split(/\s+/gum); //Split based on any amount of spaces
			await prompt.onMessage(msg, args, guildInfo, prompt)
			.catch((e) => { console.log(`Prompt error: ${e}`); });
			msg.delete().catch(() => {});
			return;
		}
	}
	let prefix = (guildInfo.commandPrefix || defaultPrefix);
	if (msg.content.substr(0,prefix.length) === prefix && /[a-z]/i.test(msg.content[prefix.length])) { //Command prefix
		let post = "";
		if (msg.content.substr(prefix.length).match(/\s+/um)) { post = msg.content.substr(prefix.length+1+msg.content.substr(prefix.length).match(/\s+/um).index); }
		let args = msg.content.substr(prefix.length).split(/\s+/gum); //Split based on any amount of spaces
		let parsed = args.shift(); //Return the first element while getting rid of it in the other array
		await commandParser(msg, parsed.toLowerCase(), args, post, prefix, guildInfo);
	}
});