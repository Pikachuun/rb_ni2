'use strict';
console.log("loading commandlist...");

//Useful command stuff
let dateParse = function (date) { //Build a custom string that includes an estimation of time spent
	let str = date.getUTCFullYear().toString();
	let now = new Date(Date.now());
	let del = now.getTime() - date.getTime();
	str += `-${(date.getUTCMonth() < 9 ? "0" : "") + (date.getUTCMonth()+1).toString()}`;
	str += `-${(date.getUTCDate() < 10 ? "0" : "") + date.getUTCDate().toString()}`;
	str += ` ${(date.getUTCHours() < 10 ? "0" : "") + date.getUTCHours().toString()}`;
	str += `:${(date.getUTCMinutes() < 10 ? "0" : "") + date.getUTCMinutes().toString()}`;
	str += `:${(date.getUTCSeconds() < 10 ? "0" : "") + date.getUTCSeconds().toString()} UTC `;
	if (now.getUTCFullYear() > date.getUTCFullYear()) { //Years (technically non-integer, must be checked manually)
		let yc = 0; let yo = date.getUTCFullYear()
		let del2 = del;
		while (del2 > 0) {
			del2 -= ((yo+yc)%4 === 0 ? 31622400000 : 31536000000); //Account for leapyears
			if (del2 >= 0) { yc++; }
		}
		if (yc > 0) {
			str += `(${yc.toString()} years ago)`;
			return str;
		}
	}
	if (now.getUTCMonth() !== date.getUTCMonth()) { //Months (extremely unpredictable and have a modulus, must be checked manually)
		let mc = 0; let mo = date.getUTCMonth(); let yo = date.getUTCFullYear(); 
		let del2 = del;
		while (del2 > 0) {
			switch ((mo+mc)%12) {
				case 1: //February
					del2 -= ((date.getUTCFullYear()+Math.floor((mo+mc)/12))%4 === 0 ? 2505600000 : 2419200000); //Account for leapyears
					break;
				case 3: //The months with 30 days
				case 5:
				case 8:
				case 10:
					del2 -= 2592000000;
					break;
				default: //All the rest have 31
					del2 -= 2678400000;
			}
			if (del2 >= 0) { mc++; }
		}
		if (mc > 0) {
			str += `(${mc.toString()} months ago)`;
			return str;
		}
	}
	if (del >= 86400000) { //Days
		str += `(${Math.floor(del/86400000).toString()} days ago)`;
	} else if (del >= 3600000) { //Hours
		str += `(${Math.floor(del/3600000).toString()} hours ago)`;
	} else if (del >= 60000) { //Minutes
		str += `(${Math.floor(del/60000).toString()} minutes ago)`;
	} else if (del >= 1000) { //Seconds
		str += `(${Math.floor(del/1000).toString()} seconds ago)`;
	} else {
		str += `(How.)`;
	}
	return str;
}
let whoisCORE = function (msg, target) {
	let embed = new MessageEmbed();
	embed.setAuthor(target.username, target.avatarURL());
	embed.setColor(0x808080);
	embed.setTitle(`User info for ${target.tag}:`);
	embed.setDescription(`
		ID: \`${target.id}\`
		Creation Date: \`${dateParse(target.createdAt)}\`
		Avatar URL: ${target.avatarURL({format:"png",dynamic:true})}
	`);
	embed.setImage(target.avatarURL({format:"png",dynamic:true}));
	msg.channel.send({embeds:[embed]});
}
let whoisGUILD = function (msg, targetG, sourceG, override = "") {
	let embed = new MessageEmbed();
	let target = targetG.user;
	let source = sourceG.user;
	let rolestr = "";
	if (targetG.roles.cache.size > 6) {
		rolestr = `${targetG.roles.cache.size-1} of them, including ${targetG.roles.highest}`;
	} else if (targetG.roles.cache.size === 1) { //@everyone
		rolestr = `None`;
	} else {
		targetG.roles.cache.each(r => {
			if (r.name !== "@everyone") { //do not include the obvious
				if (rolestr !== "") { rolestr += `, `; }
				rolestr += `\`${r.name}\``;
			}
		});
	}
	if (target.id === source.id && override === "") { override = "@self"; }
	switch (override) { //If you made the bot angry at you, you'll get sass'd
		case "@everyone":
			embed.setAuthor(sourceG.nickname && sourceG.nickname !== null ? sourceG.nickname : source.username, source.avatarURL());
			embed.setColor(parseInt(targetG.displayColor));
			embed.setTitle(targetG.nickname && targetG.nickname !== null ? `User info for ${targetG.nickname} (${target.tag}):` : `User info for ${target.tag}`);
			embed.setDescription([
				`ID: \`${target.id}\``,
				`Intelligence: \`Literally 0\``,
				`Ban Risk: \`Abnormally high\``,
				`Description: \`am i funny guys i pinged everyone for a command hahahahAHAHAH FUNNY!!!! FUNNY EVERYONE PING!!!!! TORLEEDDDDDDD!!!!! xDDDDDDDDDDDDDDDD!!!!!!!\``,
			].join('\n'));
			break;
		case "@&role":
			embed.setAuthor(sourceG.nickname && sourceG.nickname !== null ? sourceG.nickname : source.username, source.avatarURL());
			embed.setColor(parseInt(targetG.displayColor));
			embed.setTitle(targetG.nickname && targetG.nickname !== null ? `User info for ${targetG.nickname} (${target.tag}):` : `User info for ${target.tag}`);
			embed.setDescription([
				`ID: \`${target.id}\``,
				`Wimpiness: \`Infinity\``,
				`Ban Risk: \`Slightly high\``,
				`Description: \`I'm too much of a wimp to ping everyone but I pinged a group of people anyway!!!!!! please respect me :(\``,
			].join('\n'));
			break;
		case "@self":
			embed.setAuthor(sourceG.nickname && sourceG.nickname !== null ? sourceG.nickname : source.username, source.avatarURL());
			embed.setColor(parseInt(targetG.displayColor));
			embed.setTitle(targetG.nickname && targetG.nickname !== null ? `User info for ${targetG.nickname} (${target.tag}):` : `User info for ${target.tag}`);
			embed.setDescription([
				`ID: \`${target.id}\``,
				`Creation Date: \`${dateParse(target.createdAt)}\``,
				`Join Date: \`${dateParse(targetG.joinedAt)}\``,
				`Roles: ${rolestr}`,
				`Knows what \`${prefix}whoami\` is: \`false\``,
				`Avatar URL: ${target.avatarURL({format:"png",dynamic:true})}`,
			].join('\n'));
			embed.setImage(target.avatarURL({format:"png",dynamic:true}));
			break;
		default:
			embed.setAuthor(sourceG.nickname && sourceG.nickname !== null ? sourceG.nickname : source.username, source.avatarURL());
			embed.setColor(parseInt(targetG.displayColor));
			embed.setTitle(targetG.nickname && targetG.nickname !== null ? `User info for ${targetG.nickname} (${target.tag}):` : `User info for ${target.tag}`);
			embed.setDescription([
				`ID: \`${target.id}\``,
				`Creation Date: \`${dateParse(target.createdAt)}\``,
				`Join Date: \`${dateParse(targetG.joinedAt)}\``,
				`Roles: ${rolestr}`,
				`Avatar URL: ${target.avatarURL({format:"png",dynamic:true})}`,
			].join('\n'));
			embed.setImage(target.avatarURL({format:"png",dynamic:true}));
	}
	msg.channel.send({embeds:[embed]});
}
async function pixivListener (guildID, pixivSTATIC) {
	async function handleVoting (m, ci, i) {
		for (let e in pixivSTATIC.emojiList) {
			await m.react(pixivSTATIC.emojiList[e].emoji);
			//If the immediate response overflows the threshhold, skip the remainder and return
			if (m.reactions.resolve(pixivSTATIC.emojiList[e].emoji) && m.reactions.resolve(pixivSTATIC.emojiList[e].emoji).count > pixivSTATIC.threshhold) {	
				if (verbose) { console.log(`Message ID ${m.id} is already voted on, resolving...`); }
				let emojiEntry = pixivSTATIC.emojiList[e];
				let outChannel = emojiEntry.channel;
				let mo = null;
				if (outChannel === "") { //Trash case
					mo = await ci.send(`Alright, I won't forward the image with ID \`${i.id}\` then. Ripperino`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
				} else {
					try {
						let co = await CLIENT.channels.fetch(outChannel);
						await co.send(`A newly approved™ thing has arrived! ${i.title} by ${i.user.name}! Check it out:\nhttps://www.pixiv.net/artworks/${i.id}`);
						mo = await ci.send(`Nice! I forwarded it to ${co} then`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
					} catch (e) {
						console.log(e);
						mo = await ci.send(`Something went wrong in forwarding the message over. Perhaps the channel was deleted or I don't have perms?\nIf it exists, try giving me perms and reacting to the message again.`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
						setTimeout(async () => { await mo.delete().catch(() => {}); }, 5000);
						return;
					}
				}
				let pixivInfo = fetchGuildKey(guildID, "pixivInfo");
				for (let mi = 0; mi < pixivInfo.inReview.length; mi++) {
					if (pixivInfo.inReview[mi].messageID === m.id) {
						pixivInfo.inReview.splice(mi, 1);
						break;
					}
				}
				setGuildKey(guildID, "pixivInfo", pixivInfo);
				m.delete().catch(() => {});
				setTimeout(async () => { await mo.delete().catch(() => {}); }, 5000);
				return;
			}
		}
		if (verbose) { console.log(`Attaching to message ID ${m.id}...`); }
		let mc = m.createReactionCollector({filter: r => {
			if (verbose) { console.log(`Reaction to message ID ${m.id} found. Checking...`); }
			for (let e in pixivSTATIC.emojiList) {
				if ((r.emoji.url ? r.emoji.id : r.emoji.identifier) == pixivSTATIC.emojiList[e].emoji) { return true; }
			}
			return false;
		}});
		let plIndex = -1;
		for (let i = 0; i < INTERVALS.length; i++) {
			if (INTERVALS[i].guildID === m.guild.id && INTERVALS[i].type === "pixivProc") {
				plIndex = i;
				break;
			}
		}
		if (plIndex >= 0) { //Precaution
			if (!INTERVALS[plIndex].mcList) { INTERVALS[plIndex].mcList = []; } //Create a list of collectors
			INTERVALS[plIndex].mcList.push(mc); //Add this one to it for later cleanup
		}
		mc.on('collect', async r => {
			if (verbose) { console.log(`Reaction to message ID ${m.id} collected. Checking...`); }
			PIXIV.refreshAccessToken(pixivRefreshToken)
			.then(async () => {
				PIXIV.illustDetail(i.id)
				.then(async () => {
					if (r.count <= pixivSTATIC.threshhold) { return; }
					let emojiEntry = null;
					for (let e in pixivSTATIC.emojiList) {
						if ((r.emoji.url ? r.emoji.id : r.emoji.identifier) == pixivSTATIC.emojiList[e].emoji) { emojiEntry = pixivSTATIC.emojiList[e]; }
					}
					if (emojiEntry === null) { return; }
					let outChannel = emojiEntry.channel;
					let mo = null;
					if (outChannel === "") { //Trash case
						mo = await ci.send(`Alright, I won't forward the image with ID \`${i.id}\` then. Ripperino`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
					} else {
						try {
							let co = await CLIENT.channels.fetch(outChannel);
							await co.send(`A newly approved™ thing has arrived! ${i.title} by ${i.user.name}! Check it out:\nhttps://www.pixiv.net/artworks/${i.id}`);
							mo = await ci.send(`Nice! I forwarded it to ${co} then`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
						} catch (e) {
							console.log(e);
							mo = await ci.send(`Something went wrong in forwarding the message over. Perhaps the channel was deleted or I don't have perms?\nIf it exists, try giving me perms and reacting to the message again.`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
							setTimeout(async () => { await mo.delete().catch(() => {}); }, 5000);
							return;
						}
					}
					let pixivInfo = fetchGuildKey(guildID, "pixivInfo");
					for (let mi = 0; mi < pixivInfo.inReview.length; mi++) {
						if (pixivInfo.inReview[mi].messageID === m.id) {
							pixivInfo.inReview.splice(mi, 1);
							break;
						}
					}
					setGuildKey(guildID, "pixivInfo", pixivInfo);
					m.delete().catch(() => {});
					setTimeout(async () => { await mo.delete().catch(() => {}); }, 5000);
					mc.stop();
				})
				.catch(async () => {
					let mo = await ci.send(`Actually, it seems that the image with ID \`${i.id}\` was deleted, so I'll just decline it real quick. Ripperino`).catch((e) => { console.log(`${e}\nContinuing anyway...`); });
					let pixivInfo = fetchGuildKey(guildID, "pixivInfo");
					for (let mi = 0; mi < pixivInfo.inReview.length; mi++) {
						if (pixivInfo.inReview[mi].messageID == m.id) {
							pixivInfo.inReview.splice(mi, 1);
							break;
						}
					}
					setGuildKey(guildID, "pixivInfo", pixivInfo);
					m.delete().catch(() => {});
					setTimeout(async () => { await mo.delete().catch(() => {}); }, 5000);
					mc.stop();
				});
			})
			.catch((e) => { console.log(`Error occurred during image sanity checking:\n${e}\nAssuming that the image is fine and continuing...`); });
		});
		return;
	}
	if (pixivSTATIC.inReview && pixivSTATIC.inReview.length) { //RESURRECTION | only is written to if threshhold > 0 (aka processChannel is input)
		let ci = await CLIENT.channels.fetch(pixivSTATIC.processChannel);
		for (let ri = 0; ri < pixivSTATIC.inReview.length; ri++) {
			let r = pixivSTATIC.inReview[ri];
			try {
				let m = await ci.messages.fetch(r.messageID); //assume that this won't return a partial message
				let i = r.illust;
				if (pixivSTATIC.threshhold > 0) {
					await handleVoting(m, ci, i);
				} else { //threshhold was recently deleted
					ci.send(`A new illustration has arrived! ${i.title} by ${i.user.name}! Check it out:\nhttps://www.pixiv.net/artworks/${i.id}`);
					m.delete();
				}
			} catch (e) {
				console.log(`Failed to hook to message ID ${r}. Perhaps it was deleted?`);
				console.log(e);
				pixivSTATIC.inReview.splice(ri, 1); //Delete it
				ri--; //We've deleted this element, so check the new element in this position
			}
		}
		if (pixivSTATIC.threshhold < 1) {
			pixivSTATIC.inReview = []; //Clear
		}
		setGuildKey(guildID, "pixivInfo", pixivSTATIC);
	}
	let listener = setInterval(async () => {
		if (verbose) { console.log(`Proc'ing pixiv listener in guild ID ${guildID}...`); }
		let pixivInfo = fetchGuildKey(guildID, "pixivInfo");
		let searchTime = (pixivInfo.searchTime || Date.now());
		let ds = new Date(searchTime).toISOString().substr(0,10); //save some work
		let HOLP = [];
		let searchFail = false;
		await PIXIV.refreshAccessToken(pixivRefreshToken)
		.then(async () => {
			for (let t of pixivSTATIC.tags) {
				await PIXIV.searchIllust(t, {start_date:ds}) //Note: we can't search for exact times this way, use Date.parse later to do this
				.then(async json => {
					let flag = false;
					if (json && json.illusts && json.illusts.length > 0) {
						for (let i of json.illusts) {
							if (Date.parse(i.create_date) > searchTime-900000 && //Use 15 minutes as an anti-dupe precaution (in practice, some messages have gotten through on the 15 minute boundary)
								HOLP.indexOf(i.id) === -1 && pixivInfo.HOLP0.indexOf(i.id) === -1 && pixivInfo.HOLP1.indexOf(i.id) === -1 && pixivInfo.HOLP2.indexOf(i.id) === -1 //Do not execute on things we've already executed
							) {
								HOLP.push(i.id);
								if (!flag) {
									flag = true;
									if (verbose) { console.log(`New illusts found [${t}]:`); }
								}
								if (i.visible) {
									if (verbose) { console.log(`=====================================\n${i.title}(https://www.pixiv.net/artworks/${i.id}, SL=${i.sanity_level})\n"${i.caption}"`); }
									if (pixivSTATIC.filterUsers.indexOf(i.user.id) !== -1) { //Blacklist
										if (verbose) { console.log(`[BLACKLISTED USER, FILTERED OUT]"`); }
									} else if (pixivSTATIC.filterNSFW && !(i.sanity_level < 4 && !i.restrict && !i.x_restrict)) { //NSFW Suspicion; sanity_level<4 is an empirically tested bypass to restrict so omit it
										if (verbose) { console.log(`[NSFW SUSPICION, FILTERED OUT]"`); }
									} else if (pixivSTATIC.threshhold > 0) { //Voting required
										let text = `${i.user.name} uploaded a thing, ${i.title}! Vote on whether it should be forwarded to the rest of everyone or not:\nhttps://www.pixiv.net/artworks/${i.id}`;
										let ci = await CLIENT.channels.fetch(pixivSTATIC.processChannel);
										let m = await ci.send(text);
										i.searchTimeBasis = searchTime; //Test
										pixivInfo.inReview.push({messageID:m.id,illust:i});
										handleVoting(m, ci, i);
									} else {
										let co = await CLIENT.channels.fetch(pixivSTATIC.processChannel);
										co.send(`A new illustration has arrived! ${i.title} by ${i.user.name}! Check it out:\nhttps://www.pixiv.net/artworks/${i.id}`);
									}
								}	
							}
						}
					}
					if (!flag) {
						if (verbose) { console.log(`Just checked for illusts tagged with ${t}, nothing new here`); }
					}
				}).catch((e) => { console.log(`${e}\nTerminating pixiv search...`); searchFail = true; });
				if (searchFail) { break; }
			}
		}).catch((e) => {
			console.log(`Error occurred during image searching. Let's just wait for the next interval...\n${e}`);
			searchFail = true;
		});
		if (!searchFail) {
			pixivInfo.searchTime = Date.now(); //-10 minutes
			pixivInfo.HOLP2 = [...pixivInfo.HOLP1];
			pixivInfo.HOLP1 = [...pixivInfo.HOLP0];
			pixivInfo.HOLP0 = [...HOLP];
			if (verbose) { console.log(`Binding pixivInfo to guild ID ${guildID}... util.inspect:\n${util.inspect(pixivInfo)}`); }
			setGuildKey(guildID, "pixivInfo", pixivInfo);
		}
	}, 300000); //5 minutes
	if (verbose) { console.log(`Hooked a pixiv listener to guild ID ${guildID}.`); }
	INTERVALS.push({
		guildID: guildID,
		type: "pixivProc",
		interval: listener,
		mcList: [],
	});
}

//Boot/reboot cleanup
if (!DBFUNC.getAllPixivInfo) {
	DBFUNC.getAllPixivInfo = DB.prepare(`SELECT "id","pixivInfo" FROM "guilds" WHERE "pixivInfo" NOTNULL`);
}
let totalPixivInfo = DBFUNC.getAllPixivInfo.pluck(false).all();
for (let g of totalPixivInfo) {
	if (!g.pixivInfo) { continue; } //something's wrong
	pixivListener(g.id, deStringify(g.pixivInfo));
}

global.commands = {
	help: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (args && args.length === 1) {
				let commandTarget = commands[args[0].toLowerCase()];
				if (args[0].toLowerCase() === "command") {
					msg.reply("Wow, that's a funny joke. How'd you come up with that one?");
					return;
				} else if (commandTarget) {
					if (!commandTarget.helpPrompt) {
						let m = await msg.reply("There isn't any help for this command at this time, sorry.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					} else {
						let embed = commandTarget.helpPrompt(msg, new MessageEmbed().setAuthor(commandTarget.helpCategory).setColor(commandTarget.helpColor ? commandTarget.helpColor : 0x808080), prefix);
						msg.channel.send({embeds:[embed]});
						return;
					}
				} else {
					let m = await msg.reply("That isn't a valid command. Maybe `${prefix}help` would be useful for... oh yeah, this is the help command.");
					setTimeout(async () => { await m.delete(); }, 5000);
					setTimeout(() => {
						commands.help.exec(msg, [], post, prefix, guildInfo);
					}, 5000);
					return;
				}
			}
			let embed = new MessageEmbed().setTitle(`Command List`).setColor(0x808080);
			let descObj = {};
			for (let c in commands) {
				if (commands[c].helpPrompt && commands[c].helpCategory && //Ensure that the command is set up properly
					(msg.guild || !commands[c].requiredPerms) && //If it's guild-specific, ensure that it will only display in a guild
					!(commands[c].requiresBotAdmin && (!msg.channel || msg.channel.type !== "DM" || msg.author.id != botHost && botAdmins.indexOf(msg.author.id) === -1)) //Hide admin commands from non-bot admins
				) {
					if (!descObj[commands[c].helpCategory]) { descObj[commands[c].helpCategory] = []; }
					descObj[commands[c].helpCategory].push(c);
				}
			}
			let desc = ``;
			for (let d in descObj) {
				desc += `${d}: `;
				for (let c of descObj[d]) {
					if (descObj[d].indexOf(c) > 0) { desc += `,` }
					desc += `\`${c}\``;
				}
				desc += `\n`;
			}
			embed.setDescription([
				desc,
				`For help about a specific command, use \`${prefix}help command\`, where \`command\` is the command you want to learn more about.`,
			].join('\n'));
			msg.channel.send({embeds:[embed]});
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}help [command]\``);
			embed.setDescription([
				`Displays information about various commands. Much like this one!`,
				`Don't you just love recursion? I sure do.`,
				`If no argument is given, it simply lists all commands.`,
                ``,
				`If \`command\` is specified, it will list information and syntax for the specified command.`,
				`In this case, \`[]\` indicates that the argument is optional. Otherwise, it's probably required.`,
				`\`|\` indicates that any of the options separated by | can be used interchangeably.`,
				`All arguments are space-separated. `,
			].join('\n'));
			return embed;
		},
		helpCategory: "Information",
		helpColor: 0x8080c0,
	},
	whoami: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (msg.guild) {
				let guildAuthor = await msg.guild.members.fetch(msg.author.id);
				whoisGUILD(msg, guildAuthor, guildAuthor, "whoami");
			} else {
				whoisCORE(msg, msg.author);
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}whoami\``);
			embed.setDescription([
				`A command that gives you information about who you are.`,
				`Shows info such as your user ID, avatar URL, roles, and even creation/join date.`,
				`Perfect for any identity crisis you may have.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Information",
		helpColor: 0x8080c0,
	},
	whois: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (!args || args.length < 1) {
				let m = await msg.reply(`This command requires a member to be specified. Did you mean \`${prefix}whoami\`?`);
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			if (!msg.guild) {
				let m = await msg.reply(`This command needs to be used in a server.`);
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			let guildAuthor = await msg.guild.members.fetch(msg.author.id);
			//Mention handlers
			if (msg.mentions) {
				if (msg.mentions.everyone) {
					await msg.channel.send(`> Pinging everyone\n> For a whois post\nWow ${msg.author} that's so funny and hillarious :/\nI got a better one though`);
					whoisGUILD(msg, guildAuthor, guildAuthor, "@everyone");
					return;
				}
				if (msg.mentions.roles && msg.mentions.roles.size > 0) {
					await msg.channel.send(`> Pinging a role\n> For a whois post\nWow ${msg.author} that's so funny and hillarious :/\nI got a better one though`);
					whoisGUILD(msg, guildAuthor, guildAuthor, "@&role");
					return;
				}
				if (msg.mentions.channels && msg.mentions.channels.size > 0 || msg.mentions.members && msg.mentions.members.size > 1) {
					await msg.reply(`I don't really know who to pick from the list of people you gave me 🤔`);
					return;
				}
				if (msg.mentions.members && msg.mentions.members.size > 0) {
					let m = await msg.reply(`You do realize that you don't have to mention people for me to actually whois them, right?\nAh well, may as well do it anyway.`);
					setTimeout(async () => { await m.delete(); }, 5000);
					setTimeout(() => {
						whoisGUILD(msg, msg.mentions.members.first(), guildAuthor);
					}, 5000);
					return;
				}
			}
			//Otherwise, search the guild for someone with either the nick or username
			//Assume that a nickname or username will be searched for
			//
			let guildTarget = await msg.guild.members.fetch({query:post,limit:1});
			if (guildTarget.first()) {
				whoisGUILD(msg, guildTarget.first(), guildAuthor);
				return;
			} else {
				let m = await msg.reply(`I couldn't find a user with that name to whois, sorry.`);
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}whois nickname|username\``);
			embed.setColor(0x8080c0);
			embed.setDescription([
				`A command that gives you information about who someone else in a server is.`,
				`Shows info such as their user ID, avatar URL, roles, and even creation/join date.`,
				`NOTE: You don't have to ping them. Please don't be annoying.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Information",
		helpColor: 0x8080c0,
		requiredPerms: [],
	},
	roll: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			let arg = args.join("").toLowerCase();
			if (!arg) { arg = "d6"; } //by default, roll a d6
			//Dice can be broken down into various levels of expression. They're always separated by d. 
			//1d2l3h4+5
			//	i will not be implementing player's choice
			//Numbers can be replaced with any other number.
			//    2 can also include % and F.
			//+ can be replaced with any other operator (i.e. -, *, /)
			//If encapsulated in R0, all instances of 1 and 5 must be multiplied by 0.
			let rollqueue = [];
			let rolltotal = 0;
			let i = 0; let pos = 0; let len = 0;
			let argOrig = arg;
			while (arg.length) {
				let die = arg.match(/(\d*?)(d?\d+|d%|df)(?:l\d*|h\d*)*(?=$|[-+*/][\dd])/i);
				if (!die) { //There is an invalid die in here
					let m = await msg.reply("I ran into an error when parsing your roll. There seems to be an invalid die somewhere?");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				//die[1] is guaranteed to exist, but may or may not be empty
				let rollcount = (die[1] ? parseInt(die[1]) : 1);
				if (rollcount < 1) {
					let m = await msg.reply("I can't roll less than 1 die at any point. Please specify a valid dice combination.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				} else if ((rolltotal += rollcount) > 100) {
					let m = await msg.reply("I can't roll more than a total of 100 dice for safety reasons. Please specify a valid die combination.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				//die[2] is guaranteed to exist
				let rollmax = 0;
				if (die[2][0] !== "d") { //If die[2] is literally just a number, either consider it as a single die or a constant, depending on the circumstance
					if (!rollqueue.length) { //Don't put the number first!
						let m = await msg.reply("Please move your numbers to at least after the first dice roll.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					}
					rollmax = parseInt(die[2]);
					rollcount = (die[0] === argOrig ? 1 : 0);
				} else { //Otherwise, this is a standard die
					rollmax = (die[2] === "d%" ? 100 : (die[2] === "df" ? -0.1 : parseInt(die[2].substr(1))));
				}
				if (rollmax !== -0.1 && rollmax < 2) {
					let m = await msg.reply("A die cannot have less than 2 sides. Please specify a valid dice combination.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				} else if (rollcount && rollmax > 120) {
					let m = await msg.reply("I haven't ever seen a die with more than 120 sides before. Please specify a valid dice combination.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				//Check for whether we should drop the lowest results or not
				let rolldropL = 0;
				let rolldropCount = 0;
				let rolldropLTest = die[0].match(/l(\d*)/gi);
				if (rolldropLTest && rolldropLTest.length > 1) {
					let m = await msg.reply("Please specify only 1 amount of the lowest dice to drop.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				} else if (rolldropLTest && rolldropLTest.length === 1) {
					rolldropL = (rolldropLTest[0][1] ? parseInt(rolldropLTest[0][1]) : 1);
					//negative numbers are impossible, but i'll check for them anyway
					if (rolldropL < 0) {
						let m = await msg.reply("I can't drop less than 0 dice. Please specify a valid dice combination.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					} else if ((rolldropCount += rolldropL) >= rollcount) {
						let m = await msg.reply("I can't drop every dice that I roll. Please specify a valid dice combination.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					}
				}
				//Check for whether we should drop the highest results or not
				let rolldropH = 0;
				let rolldropHTest = die[0].match(/h(\d*)/gi);
				if (rolldropHTest && rolldropHTest.length > 1) {
					let m = await msg.reply("Please specify only 1 amount of the highest dice to drop.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				} else if (rolldropHTest && rolldropHTest.length === 1) {
					rolldropH = (rolldropHTest[0][1] ? parseInt(rolldropHTest[0][1]) : 1);
					//negative numbers are impossible, but i'll check for them anyway
					if (rolldropH < 0) {
						let m = await msg.reply("I can't drop less than 0 dice. Please specify a valid dice combination.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					} else if ((rolldropCount += rolldropH) >= rollcount) {
						let m = await msg.reply("I can't drop every dice that I roll. Please specify a valid dice combination.");
						setTimeout(async () => { await m.delete(); }, 5000);
						return;
					}
				}
				//Operands are after the regex, if they exist
				let rolladjunct = "";
				console.log(`testing uhhhh ${die}`);
				if (die[0].length < arg.length) {
					console.log(`next one go`);
					rolladjunct = arg[die[0].length];
					arg = arg.substr(die[0].length+1);
				} else {
					console.log(`last one go`);
					arg = "";
				}
				//Add the roll to the list
				rollqueue.push({max:rollmax, count:rollcount, dropL:rolldropL, dropH:rolldropH, adjunct:rolladjunct});
			}
			console.log(rollqueue);
			if (rollqueue === []) { //Precaution
				let m = await msg.reply("I couldn't find any dice to roll.");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			let rolls = []; let rollsM = []; let rj = -1;
			let rollSum = 0; let rollCurr = 0;
			let rollHOLP = 0; let holpOper = ""; let subFlag = false; let subFlagH = false;
			for (let ri = 0; ri < rollqueue.length; ri++) { //Actually initialize the rolls
				let r = rollqueue[ri];
				//Assume that when we reach this point there's dice to count
				rolls.push([]); rj++;
				let rc = r.count;
				rollCurr = 0; 
				if (r.max === -0.1) { //Special case: Fudge dice
					do {
						let rv = Math.floor(Math.random()*3) - 1;
						rolls[rj].push(rv);
					} while (--rc > 0);
				} else {
					do {
						let rv = Math.floor(Math.random()*r.max) + 1;
						rolls[rj].push(rv);
					} while (--rc > 0);
				}
				//Remove smallest and largest values (preserve order because i'm particular about it)
				let modRoll = rolls[rj].slice(); //shallow copy
				if (r.dropL) {
					let lc = r.dropL;
					do {
						let mindex = 0;
						let ri = 1;
						do { //This is guaranteed to have at least 2 elements due to checking bounds previously
							if (modRoll[ri] < modRoll[mindex]) { mindex = ri; }
						} while (++ri < modRoll.length)
						modRoll.splice(mindex, 1); //Drop
					} while (--lc > 0);
				}
				if (r.dropH) {
					let hc = r.dropH;
					do {
						let maxdex = 0;
						let ri = 1;
						do { //This is guaranteed to have at least 2 elements due to checking bounds previously
							if (modRoll[ri] > modRoll[maxdex]) { maxdex = ri; }
						} while (++ri < modRoll.length)
						modRoll.splice(maxdex, 1); //Drop
					} while (--hc > 0);
				}
				//NOW define the total
				rollsM.push(modRoll);
				for (let rM of modRoll) {
					rollCurr += rM;
				}
				//If there's an operator, fetch the next roll or add/multiply constants as needed
				if (r.adjunct) {
					let rni = ri+1;
					//Handle constants
					while (rni < rollqueue.length && !rollqueue[rni].count) {
						let radj = rollqueue[ri].adjunct;
						switch (radj) {
						  case "/": //division by 0 is handled earlier
						  	rollCurr /= rollqueue[rni].max;
						  	break;
						  case "*":
						  	rollCurr *= rollqueue[rni].max;
						  	break;
						  case "-":
						  	rollCurr -= rollqueue[rni].max;
						  	break;
						  default:
						  	rollCurr += rollqueue[rni].max;
						}
						rni++; ri++;
					}
					if (holpOper === "*") {
						rollHOLP *= rollCurr;
					} else if (holpOper === "/") {
						rollHOLP /= rollCurr;
					} else {
						rollSum += (subFlagH ? -rollHOLP : rollHOLP); //Add to the total
						rollHOLP = rollCurr; //Initialize
						subFlagH = subFlag;
					}
					//If a roll actually exists past this point, act depending on our curently parsed information
					if (rni < rollqueue.length) {
						let radj = rollqueue[ri].adjunct;
						switch (radj) {
						  case "/":
						  	if (rollqueue[rni].rollmax === -0.1) { //Special case: Do not divide by fudge dice, assume it's multiplication
						  		holpOper = "*";
						  		break;
						  	}
						  case "*":
						  	holpOper = radj;
						  	break;
						  case "-": //Set the subtraction flag
						  	subFlag = true;
						  	holpOper = "";
						  	break;
						  default: //Clear the subtraction flag
						  	subFlag = false;
						  	holpOper = "";
						}
					}
				} else { //Home stretch
					if (holpOper === "*") {
						rollHOLP *= rollCurr;
					} else if (holpOper === "/") {
						rollHOLP /= rollCurr;
					} else {
						rollSum += (subFlagH ? -rollHOLP : rollHOLP); //Add to the total
						rollHOLP = rollCurr; //Initialize
						subFlagH = subFlag;
					}
				}
			}
			rollSum += (subFlagH ? -rollHOLP : rollHOLP); //Since it's the end, past this point we should just add it
			if (rolltotal === 1) {
				msg.reply(`You rolled **${rollSum}**.`);
			} else {
				let embed = new MessageEmbed();
				let embedDesc = "";
				for (let r = 0; r < rolls.length; r++) {
					let sum = 0;
					for (let rv of rollsM[r]) {
						sum += rv;
					}
					embedDesc += `[${rolls[r]}]${rollsM[r].length !== rolls[r].length ? ` => [${rollsM[r]}]` : ""} = ${sum}${r === rolls.length-1 ? "" : "\n"}`;
				}
				embed.setDescription(`\`\`\`${embedDesc}\`\`\``);
				msg.reply({
					content:`The total of your roll is **${rollSum}**. See below for detailed breakdowns of each group of dice, in order.`, 
					embeds:[embed]
				});
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}roll [dice]\``);
			embed.setDescription([
				`By default, rolls a 6-sided die and outputs the result.`,
				`If \`dice\` is specified, it rolls the listed dice according to dice notation.`,
				`This tries to, but may not necessarily, obey PEDMAS.`,
				``,
				`__Examples:__`,
				`\`${prefix}roll d20\`: Rolls a 20-sided die.`,
				`\`${prefix}roll 2d10\`: Rolls 2 10-sided dice.`,
				`\`${prefix}roll d%+7\`: Rolls a 100-sided die (\`d100\` equivalent), then adds 7.`,
				`\`${prefix}roll 5d6L2*2\`: Rolls 5 6-sided dice, drops the lowest two, then multiplies the result by 2.`,
				`\`${prefix}roll 3d10H/5\`: Rolls 3 10-sided dice, drops the highest, then divides the result by 5.`,
				`\`${prefix}roll d20+3dF\`: Rolls a 20-sided die, then rolls 3 fudge dice, adding the results together.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Decisionmaking",
		helpColor: 0x60c060,
	},
	coin: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			let count = 1;
			if (args && args[0]) { count = parseInt(args[0]); }
			if (!count || count < 1 || count >= 100000) { 
				let m = await msg.reply("You either told me to flip too many coins or a non-positive amount of coins. Try a different number.");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			let heads = 0;
			if (count === 1) {
				if (Math.random() < 0.5) {
					msg.reply(`The coin came up **heads**!`);
				} else {
					msg.reply(`The coin came up **tails**.`);
				}
				return;
			} else if (count <= 50) {
		   		let results = [];
				for (let c = 0; c < count; c++) {
					if (Math.random() < 0.5) {
						heads++;
						results.push('heads');
					} else {
						results.push('tails');
					}
				}
				let embed = new MessageEmbed();
				embed.setDescription(`\`\`\`${results.join(', ')}\`\`\``);
				embed.setColor(0x404040 + Math.floor(0xbf*heads/count)*0x10000 + Math.floor(0xbf*(count-heads)/count));
				msg.reply({
					content:`Out of ${count} coins, you ended up with **${heads}** heads (${(heads/count*100).toFixed(2)}%) and **${count-heads}** tails (${((count-heads)/count*100).toFixed(2)}%).\nDetailed breakdown of results: `, 
					embeds:[embed]
				});
				return;
			} else if (count <= 625) { 
				let results = "";
				let mod = Math.ceil(Math.sqrt(count)*1.5);
				for (let c = 0; c < count; c++) {
					if (Math.random() < 0.5) {
						heads++;
						results += 'H';
					} else {
						results += 'T';
					}
					if (c%mod === mod-1) { results += '\n'; } //Such that I don't have to loop later
				}
				let embed = new MessageEmbed();
				embed.setDescription(`\`\`\`${results}\`\`\``);
				embed.setColor(0x404040 + Math.floor(0xbf*heads/count)*0x10000 + Math.floor(0xbf*(count-heads)/count));
				msg.reply({
					content:`Out of ${count} coins, you ended up with **${heads}** heads (${(heads/count*100).toFixed(2)}%) and **${count-heads}** tails (${((count-heads)/count*100).toFixed(2)}%).\nDetailed breakdown of results: `, 
					embeds:[embed]
				});
				return;
			} else {
				for (let c = 0; c < count; c++) {
					if (Math.random() < 0.5) { heads++; }
				}
				msg.reply(`Out of ${count} coins, you ended up with **${heads}** heads (${(heads/count*100).toFixed(2)}%) and **${count-heads}** tails (${((count-heads)/count*100).toFixed(2)}%).\nDetailed breakdown unavailable due to how large the input is.`);
				return;
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}coin [amount]\``);
			embed.setDescription([
				`Flip some coins.`,
				`By default, I'll only flip one coin, but if you specify \`amount\`, I'll flip that many coins (minimum 1, maximum 99999).`,
				`When more than one coin is being flipped, I'll also tell you about the percent that ended up heads and tails.`,
				`I'll also display the results for multiple flips in an embed if the count is at most 625.`,
				`After 50 coins, the embed will be in compact mode.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Decisionmaking",
		helpColor: 0x60c060,
	},
	guildconfig: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (args && args.length === 2) {
				switch (args[0].toLowerCase()) {
					case "prefix":
						if (args[1].toLowerCase() == "default") {
							delete guildInfo.commandPrefix; //Clear
							let m = await msg.reply(`Alright. From now on, please use the default prefix, \`${defaultPrefix}\`, when calling commands.`);
							setTimeout(async () => { await m.delete(); }, 5000);
						} else {
							guildInfo.commandPrefix = args[1]; delete("commandPrefix"); //Clear
							let m = await msg.reply(`Done. From now on, please use the specified prefix, \`${args[1]}\`, when calling commands.`);
							setTimeout(async () => { await m.delete(); }, 5000);
						}
						setGuildKey(msg.guild.id, "mainInfo", guildInfo);
						break;
					default:
						let m = await msg.reply(`The setting ${args[0]} doesn't exist, so I can't set it, sorry.`);
						setTimeout(async () => { await m.delete(); }, 5000);
				}
			} else if (args && args.length === 1) {
				switch (args[0].toLowerCase()) {
					case "prefix":
						if (guildInfo.commandPrefix) {
							msg.reply(`As you guessed correctly, the command prefix in this guild is \`${guildInfo.commandPrefix}\`.\nI'll leave this message up just in case anyone else asks.`);
						} else {
							msg.reply(`As you guessed correctly, the command prefix in this guild is \`${defaultPrefix}\` (the default one).\nI'll leave this message up just in case anyone else asks.`)
						}
						break;
					default:
						let m = await msg.reply(`The setting ${args[0]} doesn't exist, so I can't get it for you, sorry.`);
						setTimeout(async () => { await m.delete(); }, 5000);
				}
			} else {
				let m = await msg.reply(`You either didn't specify a setting or specified too many arguments.\nKeep in mind that I can't set anything to a value with spaces because arguments are space-separated...`);
				setTimeout(async () => { await m.delete(); }, 5000);
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}guildconfig setting [value]\``);
			embed.setDescription([
				`__**REQUIRES GUILD ADMIN PERMISSIONS**__`,
				`If \`value\` is specified, changes the setting specified to that value. Otherwise, it's just displayed.`,
				`Currently supported settings are the following.`,
				`\* \`prefix\`: Sets the bot's command prefix. A value of \`default\` will reset it to its default.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Guild Management",
		helpColor: 0xc06060,
		requiredPerms: [DISCORD.Permissions.FLAGS.ADMINISTRATOR],
	},
	pixiv: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (!pixivRefreshToken) {
				let m = await msg.reply("Since a pixiv auth token wasn't defined in the bot's config, I don't think I'll be able to handle this reliably...");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			//Deletion
			if (args && args[0] && args[0].toLowerCase() === "delete") {
				let del = -1;
				for (let i = 0; i < INTERVALS.length; i++) {
					if (INTERVALS[i].guildID === msg.guild.id && INTERVALS[i].type === "pixivProc") {
						del = i;
						break;
					}
				}
				if (del < 0) {
					let m = await msg.reply("There don't seem to be any pixiv listeners to delete?");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				} else {
					if (INTERVALS[del].mcList && INTERVALS[del].mcList.length) { //Also terminate all voting that may exist
						for (let c = 0; c < INTERVALS[del].mcList.length; c++) {
							INTERVALS[del].mcList[c].stop();
						}
					}
					clearInterval(INTERVALS[del].interval);
					INTERVALS.splice(del, 1);
					delGuildKey(msg.guild.id, "pixivInfo"); //Additionally, clear pixivInfo
					let m = await msg.reply("I've deleted the pixiv listener for this guild.");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				return;
			//DNE
			} else if (!(args && args[0] && args[0].toLowerCase() === "setup")) {
				let m = await msg.reply("Please specify either `setup` or `delete`.");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			//If an interactive prompt is active, do nothing
			let prompt = PROMPTS[msg.guild.id];
			if (prompt && prompt.isActive) {
				let m = await msg.reply("I'm already handling a prompt somewhere else in the guild, so I can't do that right now. Sorry.");
				setTimeout(async () => { await m.delete(); }, 5000);
				return;
			}
			//Init
			let initPixivInfo = fetchGuildKey(msg.guild.id, "pixivInfo") || {
				tags: [],
				filterNSFW: true,
				filterUsers: [],
				processChannel: msg.channel.id,
				threshhold: 0,
				emojiList: {},
				HOLP0: [],
				HOLP1: [],
				HOLP2: [],
				inReview: [],
			};
			let embed = new MessageEmbed().setColor(0x0040ff);
			embed.setTitle("PIXIV LISTENER SETUP");
			embed.setDescription([
				`Search Tags: \`${JSON.stringify(initPixivInfo.tags)}\``,
				`Blacklist NSFW? \`${initPixivInfo.filterNSFW}\``,
				`UserID Blacklist: \`${JSON.stringify(initPixivInfo.filterUsers)}\``,
				`Input Channel: \`${initPixivInfo.processChannel}\``,
				`Voting Threshhold: \`${initPixivInfo.threshhold}\``,
				`Emoji Count: \`${Object.keys(initPixivInfo.emojiList).length}\``,
				``,
				`**Commands:**`,
				`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
				`\`exit\`: Cancels the setup process without saving.`,
				`\`tags add tag\`: Add a search tag.`,
				`\`tags clear\`: Remove all search tags.`,
				`\`tags delete tag\`: Remove a particular search tag.`,
				`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
				`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
				`\`userblacklist clear\`: Clear the user blacklist.`,
				`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
				`\`setchannel #channel\`: Set the channel to forward search results to.`,
				`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
				`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
				`\`votereact clear\`: Remove all current voting options.`,
				`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
			].join('\n'));
			let coreMsg = await msg.channel.send({embeds:[embed]});
			//Setting up the prompt
			PROMPTS[msg.guild.id] = {
				isActive: true,
				promptee: msg.author.id,
				channel: msg.channel.id,
				promptState: 0,
				pixivInfo: initPixivInfo,
				secondMessage: null,
				secondMessageCollector: null,
				onMessage: async function (msg, args, guildInfo, prompt) {
					if (msg.content.toLowerCase() === "save") { //Save and (re-)deploy
						//Idiot-proofing
						//We need at least 1 tag to search
						if (prompt.pixivInfo.tags.length < 1) {
							let m = await msg.channel.send("What am I searching for, exactly?\nSpecify at least one tag using `tags add` before saving.");
							setTimeout(async () => { await m.delete(); }, 5000);
							return;
						}
						//If voting, needs at least 1 reaction somewhere (it doesn't have to be trash)
						if (prompt.pixivInfo.threshhold > 0 && Object.keys(prompt.pixivInfo.emojiList).length < 1) {
							let m = await msg.channel.send("You need at least 1 output channel if you're voting, even if it's just a trash can, right?\nSpecify one using `votereact add` before saving.");
							setTimeout(async () => { await m.delete(); }, 5000);
							return;
						}
						//Purge any old listeners if they exist
						let del = -1;
						for (let i = 0; i < INTERVALS.length; i++) {
							if (INTERVALS[i].guildID === msg.guild.id && INTERVALS[i].type === "pixivProc") {
								del = i;
								break;
							}
						}
						if (del >= 0) {
							if (INTERVALS[del].mcList && INTERVALS[del].mcList.length) { //Also terminate all voting that may exist
								for (let c = 0; c < INTERVALS[del].mcList.length; c++) {
									INTERVALS[del].mcList[c].stop();
								}
							}
							clearInterval(INTERVALS[del].interval);
						}
						setGuildKey(msg.guild.id, "pixivInfo", prompt.pixivInfo);
						pixivListener(msg.guild.id, prompt.pixivInfo);
						let m = await msg.channel.send("The pixiv listener has been saved and deployed successfully.");
						setTimeout(async () => { await m.delete(); }, 5000);
						coreMsg.delete().catch(() => {});
						if (prompt.secondMessage) {
							try {
								let m2 = await msg.channel.messages.fetch(prompt.secondMessage);
								m2.delete().catch(() => {});
							} catch (e) {

							}
						}
						delete PROMPTS[msg.guild.id];
						return;
					}
					if (msg.content.toLowerCase() === "exit") { //Forced Termination
						let m = await msg.channel.send("pixiv listener setup has been cancelled.");
						setTimeout(async () => { await m.delete(); }, 5000);
						coreMsg.delete().catch(() => {});
						if (prompt.secondMessage) {
							try {
								let m2 = await msg.channel.messages.fetch(prompt.secondMessage);
								m2.delete().catch(() => {});
							} catch (e) {

							}
						}
						delete PROMPTS[msg.guild.id];
						return;
					}
					switch (prompt.promptState) {
						case 0: //main
							switch (args[0].toLowerCase()) {
								case "setchannel":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify a channel for me to forward all initial announcements to, and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									if (msg.mentions && msg.mentions.channels) {
										if (msg.mentions.channels.first() !== msg.mentions.channels.last()) {
											let m = await msg.channel.send("Just 1 channel, please.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										} else {
											prompt.pixivInfo.processChannel = msg.mentions.channels.first().id;
											PROMPTS[msg.guild.id] = prompt;
											embed.setDescription([
												`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
												`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
												`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
												`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
												`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
												`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
												``,
												`**Commands:**`,
												`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
												`\`exit\`: Cancels the setup process without saving.`,
												`\`tags add tag\`: Add a search tag.`,
												`\`tags clear\`: Remove all search tags.`,
												`\`tags delete tag\`: Remove a particular search tag.`,
												`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
												`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
												`\`userblacklist clear\`: Clear the user blacklist.`,
												`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
												`\`setchannel #channel\`: Set the channel to forward search results to.`,
												`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
												`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
												`\`votereact clear\`: Remove all current voting options.`,
												`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
											].join('\n'));
											coreMsg.edit({embeds:[embed]});
											let m = await msg.channel.send(`Set the input channel to ${msg.mentions.channels.first()}.\nPlease input another command.`);
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
									} else {
										let m = await msg.channel.send("Please specify a channel for me to forward all initial announcements to, and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									break;
								case "threshhold":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify a non-negative number to set the voting threshhold to and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									let threshhold = parseInt(args[1]);
									if (isNaN(threshhold) || threshhold < 0) {
										let m = await msg.channel.send("Please specify a non-negative number to set the voting threshhold to and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									prompt.pixivInfo.threshhold = threshhold;
									PROMPTS[msg.guild.id] = prompt;
									embed.setDescription([
										`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
										`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
										`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
										`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
										`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
										`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
										``,
										`**Commands:**`,
										`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
										`\`exit\`: Cancels the setup process without saving.`,
										`\`tags add tag\`: Add a search tag.`,
										`\`tags clear\`: Remove all search tags.`,
										`\`tags delete tag\`: Remove a particular search tag.`,
										`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
										`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
										`\`userblacklist clear\`: Clear the user blacklist.`,
										`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
										`\`setchannel #channel\`: Set the channel to forward search results to.`,
										`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
										`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
										`\`votereact clear\`: Remove all current voting options.`,
										`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
									].join('\n'));
									coreMsg.edit({embeds:[embed]});
									if (threshhold === 0) {
										let m = await msg.channel.send(`Voting has been disabled. All messages will be forwarded directly to the input channel.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else {
										let m = await msg.channel.send(`Voting has been enabled, with ${threshhold} successful votes required to forward it to a particular channel.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									break;
								case "votereact":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify a subcommand such that I can properly handle it.\nValid subcommands are \`add\`, \`clear\`, and \`delete\`.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									if (args[1].toLowerCase() === "add") {
										if (args.length < 3) {
											let m = await msg.channel.send("Please specify a channel and try again.\nYou can also type \`trash\` instead to treat it as a sort of downvote.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let emojiList = prompt.pixivInfo.emojiList;
										let heldChannel = "";
										if (msg.mentions && msg.mentions.channels && msg.mentions.channels.first()) {
											if (msg.mentions.channels.first() !== msg.mentions.channels.last()) {
												let m = await msg.channel.send("Just 1 channel, please.");
												setTimeout(async () => { await m.delete(); }, 5000);
												return;
											} else {
												heldChannel = msg.mentions.channels.first().id;
											}
										} else if (args[2].toLowerCase() !== "trash") {
											let m = await msg.channel.send("Please specify a channel and try again.\nYou can also type \`trash\` instead to treat it as a sort of downvote.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let m = await msg.channel.send("Please react to this message with the emoji that you wish to use as a marker to send it to that channel.");
										let mc = m.createReactionCollector({filter: async (r, u) => {
											if (r.count === 1 && !r.createChecked) { //HACKY WORKAROUND, PLEASE REMOVE WHEN FIXED
												r.createChecked = true;
												return false;
											} 
											if (verbose) { console.log(`Checking reaction:\n${util.inspect(r)}`); }
											if (u.id !== prompt.promptee) { return false; } //Must be the promptee
											let emojiFound = false;
											for (let e in emojiList) {
												if (r.emoji.id == emojiList[e].emoji) {
													emojiFound = true;
													break;
												}
											}
											if (emojiFound) {
												let m2 = await msg.channel.send("This emoji already exists, so I'll just replace its destination with the channel that you specified.\nPlease input another command.");
												setTimeout(async () => { await m2.delete(); }, 5000);
												return true;
											} else {
												if (r.emoji.url) { //custom emoji. attempt to access it
													if (verbose) { console.log(`Checking custom emoji at ${r.emoji.url} with ID ${r.emoji.id}...`); }
													let ge = await CLIENT.emojis.resolve(r.emoji.id);
													if (!ge) {
														let m2 = await msg.channel.send("I can't seem to access that emoji. Try another?");
														setTimeout(async () => { await m2.delete(); }, 5000);
														return false;
													}
												}
												let m2 = await msg.channel.send("Added the emoji to the list.\nPlease input another command.");
												setTimeout(async () => { await m2.delete(); }, 5000);
												return true;
											}
										}});
										prompt.secondMessage = m.id;
										prompt.promptState = 1; //Enter Reaction Acquisition Mode
										PROMPTS[msg.guild.id] = prompt;
										mc.on("collect", async (r, u) => {
											if (verbose) { console.log(`Reaction passed.`); }
											emojiList[r.emoji.url ? r.emoji.id : r.emoji.identifier] = {
												emoji: r.emoji.url ? r.emoji.id : r.emoji.identifier,
												channel: heldChannel,
											}
											prompt.pixivInfo.emojiList = emojiList;
											prompt.secondMessage = null;
											prompt.promptState = 0; //Return to Command Input Mode
											PROMPTS[msg.guild.id] = prompt;
											embed.setDescription([
												`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
												`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
												`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
												`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
												`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
												`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
												``,
												`**Commands:**`,
												`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
												`\`exit\`: Cancels the setup process without saving.`,
												`\`tags add tag\`: Add a search tag.`,
												`\`tags clear\`: Remove all search tags.`,
												`\`tags delete tag\`: Remove a particular search tag.`,
												`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
												`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
												`\`userblacklist clear\`: Clear the user blacklist.`,
												`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
												`\`setchannel #channel\`: Set the channel to forward search results to.`,
												`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
												`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
												`\`votereact clear\`: Remove all current voting options.`,
												`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
											].join('\n'));
											coreMsg.edit({embeds:[embed]});
											m.delete();
											mc.stop();
										});
										return;
									} else if (args[1].toLowerCase() === "clear") {
										prompt.pixivInfo.emojiList = {};
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send("Cleared the currently existing emoji database.\nPlease input another command.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "delete") {
										let emojiList = prompt.pixivInfo.emojiList;
										if (Object.keys(emojiList).length < 1) {
											let m = await msg.channel.send("There are no emojis to delete from the database!");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let m = await msg.channel.send("Please react to this message with the emoji that you wish to delete.\nIn case you need a reminder, here's a list (please wait for all of them to show up):");
										for (let e in emojiList) { await m.react(emojiList[e].emoji); }
										let mc = m.createReactionCollector({filter: async (r, u) => {
											if (u.id !== prompt.promptee) { return false; } //Must be the promptee
											let emojiFound = false;
											for (let e in emojiList) {
												if (r.emoji.id == emojiList[e].emoji) {
													emojiFound = true;
													break;
												}
											}
											if (emojiFound) {
												let m2 = await msg.channel.send("Deleted the emoji from the database.\nPlease input another command.");
												setTimeout(async () => { await m2.delete(); }, 5000);
											} else {
												let m2 = await msg.channel.send("That emoji doesn't exist in the database! Please react with one of the emojis that are.");
												setTimeout(async () => { await m2.delete(); }, 5000);
											}
											return emojiFound;
										}});
										prompt.secondMessage = m.id;
										prompt.promptState = 1; //Enter Reaction Acquisition Mode
										PROMPTS[msg.guild.id] = prompt;
										mc.on("collect", async (r, u) => {
											delete emojiList[r.emoji.url ? r.emoji.id : r.emoji.identifier];
											prompt.pixivInfo.emojiList = emojiList;
											prompt.secondMessage = null;
											prompt.promptState = 0; //Return to Command Input Mode
											PROMPTS[msg.guild.id] = prompt;
											embed.setDescription([
												`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
												`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
												`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
												`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
												`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
												`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
												``,
												`**Commands:**`,
												`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
												`\`exit\`: Cancels the setup process without saving.`,
												`\`tags add tag\`: Add a search tag.`,
												`\`tags clear\`: Remove all search tags.`,
												`\`tags delete tag\`: Remove a particular search tag.`,
												`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
												`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
												`\`userblacklist clear\`: Clear the user blacklist.`,
												`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
												`\`setchannel #channel\`: Set the channel to forward search results to.`,
												`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
												`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
												`\`votereact clear\`: Remove all current voting options.`,
												`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
											].join('\n'));
											coreMsg.edit({embeds:[embed]});
											m.delete();
											mc.stop();
										});
										return;
									}
									break;
								case "tags":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify a subcommand such that I can properly handle it.\nValid subcommands are \`add\`, \`clear\`, and \`delete\`.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									if (args[1].toLowerCase() === "add") {
										if (args.length < 3) {
											let m = await msg.channel.send("Please specify a tag that you want me to search for, and try again.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let tags = prompt.pixivInfo.tags;
										if (tags.indexOf(args[2].toLowerCase()) > -1) {
											let m = await msg.channel.send("This tag is already being searched for!");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										tags.push(args[2].toLowerCase());
										prompt.pixivInfo.tags = tags;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send(`Alright, I will now search for the tag ${args[2]}.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "clear") {
										prompt.pixivInfo.tags = [];
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send("Cleared the currently existing tag list.\nPlease input another command.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "delete") {
										if (args.length < 3) {
											let m = await msg.channel.send("Please specify a tag that you want me to stop searching for, and try again.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let tags = prompt.pixivInfo.tags;
										if (tags.length < 1) {
											let m = await msg.channel.send("There are no tags to delete!");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										if (tags.indexOf(args[2].toLowerCase()) === -1) {
											let m = await msg.channel.send("We aren't searching for this tag! As a reminder, here's a list of tags that we *are* searching for:\n" + JSON.stringify(tags));
											setTimeout(async () => { await m.delete(); }, 10000);
											return;
										}
										tags.splice(tags.indexOf(args[2].toLowerCase()), 1);
										prompt.pixivInfo.tags = tags;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send(`Alright, I will no longer search for the tag ${args[2]}.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else {
										let m = await msg.channel.send("Please specify a subcommand such that I can properly handle it.\nValid subcommands are \`add\`, \`clear\`, and \`delete\`.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									break;
								case "filternsfw":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify whether I should (\`true\`) or shouldn't (\`false\`) **exclude** NSFW searches, and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									if (args[1].toLowerCase() === "true") {
										prompt.pixivInfo.filterNSFW = true;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send("Alright, I will now **exclude** NSFW searches in my results.\nPlease input another command.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "false") {
										prompt.pixivInfo.filterNSFW = false;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send("Alright, I will now **include** NSFW searches in my results.\nPlease input another command.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else {
										let m = await msg.channel.send("Please specify whether I should (\`true\`) or shouldn't (\`false\`) **exclude** NSFW searches, and try again.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									break;
								case "userblacklist":
									if (args.length < 2) {
										let m = await msg.channel.send("Please specify a subcommand such that I can properly handle it.\nValid subcommands are \`add\`, \`clear\`, and \`delete\`.\nNote that I refer to user **IDs**, not names.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									if (args[1].toLowerCase() === "add") {
										if (args.length < 3 || isNaN(parseInt(args[2]))) {
											let m = await msg.channel.send("Please specify a user ID that you want me to blacklist, and try again.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let filterUsers = prompt.pixivInfo.filterUsers;
										if (filterUsers.indexOf(parseInt(args[2])) > -1) {
											let m = await msg.channel.send("This user is already being blacklisted!");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										filterUsers.push(parseInt(args[2]));
										prompt.pixivInfo.filterUsers = filterUsers;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send(`Alright, I will now blacklist the user with the ID ${parseInt(args[2])}.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "clear") {
										prompt.pixivInfo.filterUsers = [];
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send("Cleared the currently existing user blacklist.\nPlease input another command.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else if (args[1].toLowerCase() === "delete") {
										if (args.length < 3 || isNaN(parseInt(args[2]))) {
											let m = await msg.channel.send("Please specify a user ID that you want me to stop blacklisting, and try again.");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										let filterUsers = prompt.pixivInfo.filterUsers;
										if (filterUsers.length < 1) {
											let m = await msg.channel.send("There are no blacklisted users!");
											setTimeout(async () => { await m.delete(); }, 5000);
											return;
										}
										if (filterUsers.indexOf(parseInt(args[2])) === -1) {
											let m = await msg.channel.send("We aren't blacklisting this user! As a reminder, here's a list of users that we *are* blacklisting:\n" + JSON.stringify(filterUsers));
											setTimeout(async () => { await m.delete(); }, 10000);
											return;
										}
										filterUsers.splice(filterUsers.indexOf(parseInt(args[2])), 1);
										prompt.pixivInfo.filterUsers = filterUsers;
										PROMPTS[msg.guild.id] = prompt;
										embed.setDescription([
											`Search Tags: \`${JSON.stringify(prompt.pixivInfo.tags)}\``,
											`Blacklist NSFW? \`${prompt.pixivInfo.filterNSFW}\``,
											`UserID Blacklist: \`${JSON.stringify(prompt.pixivInfo.filterUsers)}\``,
											`Input Channel: \`${prompt.pixivInfo.processChannel}\``,
											`Voting Threshhold: \`${prompt.pixivInfo.threshhold}\``,
											`Emoji Count: \`${Object.keys(prompt.pixivInfo.emojiList).length}\``,
											``,
											`**Commands:**`,
											`\`save\`: Saves and (re)starts the pixiv listener for the guild.`,
											`\`exit\`: Cancels the setup process without saving.`,
											`\`tags add tag\`: Add a search tag.`,
											`\`tags clear\`: Remove all search tags.`,
											`\`tags delete tag\`: Remove a particular search tag.`,
											`\`blacklistnsfw value\`: Toggle whether NSFW results should be shown or not.`,
											`\`userblacklist add userID\`: Blacklist a particular user's results from showing up.`,
											`\`userblacklist clear\`: Clear the user blacklist.`,
											`\`userblacklist delete userID\`: Remove a user from the blacklist.`,
											`\`setchannel #channel\`: Set the channel to forward search results to.`,
											`\`threshhold value\`: If voting is necessary, set the amount of votes needed to forward the result somewhere else.`,
											`\`votereact add #channel\`: Add a voting option that forwards the result to a particular channel. \`trash\` can be specified instead of a channel to discard it.`,
											`\`votereact clear\`: Remove all current voting options.`,
											`\`votereact delete\`: Delete a particular voting option, based on a subsequent emoji.`,
										].join('\n'));
										coreMsg.edit({embeds:[embed]});
										let m = await msg.channel.send(`Alright, I will no longer blacklist the user with ID ${parseInt(args[2])}.\nPlease input another command.`);
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									} else {
										let m = await msg.channel.send("Please specify a subcommand such that I can properly handle it.\nValid subcommands are \`add\`, \`clear\`, and \`delete\`.\nNote that I refer to user **IDs**, not names.");
										setTimeout(async () => { await m.delete(); }, 5000);
										return;
									}
									break;
								default:
									let m = await msg.channel.send("Invalid command. Please keep in mind that I keep track of **all** messages you send in this channel.\nIf you want to end setup, type \`exit\`.");
									setTimeout(async () => { await m.delete(); }, 5000);
									return;
							}
							break;
						case 1: //votereact.add/del
							let m = await msg.channel.send("React to my above message, please.");
							setTimeout(async () => { await m.delete(); }, 5000);
							return;
					}
				},
			};
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}pixiv subcommand\``);
			embed.setDescription([
				`__**REQUIRES GUILD ADMIN PERMISSIONS**__`,
				`Does various things with the guild's pixiv listener, depending on \`subcommand.\``,
				`\* \`setup\`: Creates a pixiv listener if it doesn't already exist, or edits any existing ones. It'll be explained as you go along.`,
				`\* \`delete\`: Removes the pixiv listener. **THIS WILL ALSO CAUSE ALL ACTIVE VOTING PROCESSES TO STOP.**`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Guild Management",
		helpColor: 0xc06060,
		requiredPerms: [DISCORD.Permissions.FLAGS.ADMINISTRATOR],
	},
	reload: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			console.log("Reloading...");
			BOOT(true);
			msg.react("👍");
			console.log("Reload complete.");
			if (args && args.length > 0) { 
				let reparsed = args.shift(); //Return the first element while getting rid of it in the other array
				commandParser(msg, reparsed.toLowerCase(), args, post, prefix);
			}
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}reload [command]\``);
			embed.setDescription([
				`__**REQUIRES BOT ADMIN PERMISSIONS**__`,
				`Reloads mostly everything related to the bot. This includes the following.`,
				`\* Configuration`,
				`\* Commands`,
				`\* Message/Other Client Processing`,
				`\* The pixiv voting system`,
				``,
				`As a convenience feature, if \`command\` is specified, it will execute the command immediately after reloading.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Admin",
		helpColor: 0xff0000,
		requiresBotAdmin: true,
	},
	eval: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			if (!args) { return; }
			let evalCode = post.match(/```((.|\n)+?)```/imu);
			if (!evalCode || evalCode.length < 2 || evalCode[1].length < 1) {
				msg.reply("Use the \\`\\`\\`Cool Quotes\\`\\`\\` to define some code");
				return;
			}
			let out = eval(evalCode[1]);
			console.log(`Eval results:\n${out}`);
			msg.react("👍");
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}eval code\``);
			embed.setDescription([
				`__**REQUIRES BOT ADMIN PERMISSIONS**__`,
				`Executes \`code\`, exactly how it's written, however node.js would parse it.`,
				`As a precaution, it will only parse code in \\\`\\\`\\\`actual code quotes.\\\`\\\`\\\``,
				`**Only use this command if you know what you're doing.**`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Admin",
		helpColor: 0xff0000,
		requiresBotAdmin: true,
	},
	push: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			let gitCode = EXEC(`git -C ${path.resolve("")} add . && git -C ${path.resolve("")} commit -m "${post}" && git -C ${path.resolve("")} push origin master`, {shell:true});
			gitCode.stdout.on('data', d => {
				console.log(d.toString());
			});
			gitCode.stderr.on('data', d => {
				console.log(d.toString());
			});
			gitCode.on('exit', async e => {
				if (parseInt(e) !== 0) {
					let m = await msg.reply("Something didn't push properly");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				msg.react("👍");
			});
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}push [commitMessage]\``);
			embed.setDescription([
				`__**REQUIRES BOT ADMIN PERMISSIONS**__`,
				`Pushes the changes made to the bot to the git repository it's associated with.`,
				`For the sake of remote updating.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Admin",
		helpColor: 0xff0000,
		requiresBotAdmin: true,
	},
	pull: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			let gitCode = EXEC(`git -C ${path.resolve("")} fetch origin master && git -C ${path.resolve("")} pull origin master`, {shell:true});
			gitCode.stdout.on('data', d => {
				console.log(d.toString());
			});
			gitCode.stderr.on('data', d => {
				console.log(d.toString());
			});
			gitCode.on('exit', async e => {
				if (parseInt(e) !== 0) {
					let m = await msg.reply("Something didn't pull properly");
					setTimeout(async () => { await m.delete(); }, 5000);
					return;
				}
				msg.react("👍");
				//Immediately reload
				commandParser(msg, "reload", [], "");
			});
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}pull\``);
			embed.setDescription([
				`__**REQUIRES BOT ADMIN PERMISSIONS**__`,
				`Pulls the changes made to the bot to the git repository it's associated with, then immediately reloads the bot.`,
				`For the sake of remote updating.`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Admin",
		helpColor: 0xff0000,
		requiresBotAdmin: true,
	},
	shutdown: {
		exec: async function (msg, args, post, prefix, guildInfo) {
			console.log("Shutting down...");
			await msg.react("😔");
			await msg.react("🤘");
			CLIENT.destroy();
			process.exit(0);
		},
		helpPrompt: function (msg, embed, prefix) {
			embed.setTitle(`\`${prefix}shutdown\``);
			embed.setDescription([
				`__**REQUIRES BOT ADMIN PERMISSIONS**__`,
				`Shuts me down graciously, without any errors.`,
				`Most of the time I shut down because of crashes but at least let me sleep normally once in a while okay? 😔🤘`,
			].join('\n'));
			return embed;
		},
		helpCategory: "Admin",
		helpColor: 0xff0000,
		requiresBotAdmin: true,
	},
}
